from src.manager.CreateCode2VecSets import create_datasets_from
from src.manager.manageDataSet import *


logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)

########################################################################################################################
###### READ ME TO UNDERSTAND WHAT HAPPEN IN THIS PYTHON SCRIPT IN A SHORT EXPLANATION
# It script is to generate the data in input of the PHP/NODEJS JAVA PARSER creating the set of AST paths to code2vec preprocess
# What you have to change to run this script in order to shuffle and divide on trainset 69%, validationset 30% and testset 1% of the trainset
# is only the config part such as :
# --- the path of the PHP/NodeJs dataset
# --- the path of the new folders where would like put the train + test + valid dataset
# This script is optimized to use threads
########################################################################################################################
###### CONFIG PART

WITH_R_R_R___ = 'total'


LANG = 'HOP'
LANG = 'NODEJS'

PERCENT =15 #%
PERCENT_FOR_TRAIN =70 #%


DATASET_NAME = '%s-renamedVariable-%s-with-HTML-for-D1' % (LANG, WITH_R_R_R___)
# DATASET_NAME = '%s-renamedVariable-%s-template-with-HTML-for-D2' % (LANG, WITH_R_R_R___)
DATASET = '/aboslute/path/db/%s/' % DATASET_NAME #I can used for word2vec like that


NEW_DATASET_NAME = '%s-final-preprocessed-renamedVariable-%s-with-HTML-D1' % (LANG, WITH_R_R_R___)


db_version = 'db-v1'
NEW_DATASET_PATH = '../../../db/%s/%s' % (db_version, NEW_DATASET_NAME)

TRAIN_SET_PATH = os.path.join(NEW_DATASET_PATH,'trainset_files/')
TEST_SET_PATH =  os.path.join(NEW_DATASET_PATH,'testset_files/')
VALID_SET_PATH =  os.path.join(NEW_DATASET_PATH,'validset_files/')

JSON_CONFIG_PATH = '../../db-config/%s' % db_version
JSON_PATH = '%s/%s.json' % (JSON_CONFIG_PATH, NEW_DATASET_NAME)



########################################################################################################################
####### DO IT FOR CREATE VALID AND THE TESTING  DATASET ####################################################################
percent_ = PERCENT / 100
purcentage_0 = PERCENT_FOR_TRAIN / 100

logging.info("DATASET --> %s", DATASET)
logging.info("purcentage_train --> %s", purcentage_0)
logging.info("purcentage_test --> %s", percent_)
logging.info("purcentage_valid --> %s", percent_)
logging.info("json_config_path --> %s", JSON_CONFIG_PATH)
logging.info("TRAIN_SET_PATH --> %s", TRAIN_SET_PATH)
logging.info("VALID_SET_PATH --> %s", VALID_SET_PATH)
logging.info("JSON_PATH --> %s", JSON_PATH)
create_datasets_from(dataset=DATASET, purcentage_train=purcentage_0, purcentage_test=percent_, purcentage_valid=percent_,
                     func_to_divide_database_in_datasets=divide_database_in_3_sets, json_config_path=JSON_CONFIG_PATH,
                     train_set_path=TRAIN_SET_PATH, test_set_path=TEST_SET_PATH, valid_set_path=VALID_SET_PATH, json_path=JSON_PATH)

