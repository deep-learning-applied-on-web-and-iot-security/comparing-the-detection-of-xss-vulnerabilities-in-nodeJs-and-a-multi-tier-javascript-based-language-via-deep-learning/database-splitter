from src.manager.CreateCode2VecSets import create_datasets_from
from src.manager.manageDataSet import *


logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)

########################################################################################################################
###### READ ME TO UNDERSTAND WHAT HAPPEN IN THIS PYTHON SCRIPT IN A SHORT EXPLANATION
# It script is to generate the data in input of the PHP/NODEJS JAVA PARSER creating the set of AST paths to code2vec preprocess
# What you have to change to run this script in order to shuffle and divide on trainset 69%, validationset 30% and testset 1% of the trainset
# is only the config part such as :
# --- the path of the PHP/NodeJs dataset
# --- the path of the new folders where would like put the train + test + valid dataset
# This script is optimized to use threads
########################################################################################################################
###### CONFIG PART
WITH_R_R_R___ = 'r0-r1-r2-r5_14'



LANG = 'HOP'
LANG = 'NODEJS'
PERCENT =15 #%
PERCENT_FOR_TRAIN =70 #%


DATASET_NAME = '%s-renamedVariable-data-%s-template-with-HTML-for-D1' % (LANG, WITH_R_R_R___)
# DATASET_NAME = '%s-renamedVariable-data-%s-template-with-HTML-for-D2' % (LANG, WITH_R_R_R___)
DATASET = '/absolute/path/db/%s/' % DATASET_NAME #I can used for word2vec like that

# NEW_DATASET_NAME = '%s-renamedVariable-data-%s-template_SPLIT_IN_TEST_VALID-with-HTML-D1' % (LANG, WITH_R_R_R___)
# NEW_DATASET_NAME = '%s-renamedVariable-data-%s-template_SPLIT_IN_TEST_VALID-with-HTML-D2' % (LANG, WITH_R_R_R___)
NEW_DATASET_NAME = '%s-renamedVariable-data-%s-template-SPLIT_IN_TEST_VALID-with-HTML-D1-%spc' % (LANG, WITH_R_R_R___, PERCENT)

db_version = 'db-v8'
db_version = 'db-v9'
NEW_DATASET_PATH = '../../../db/%s/%s' % (db_version, NEW_DATASET_NAME)

TRAIN_SET_PATH = os.path.join(NEW_DATASET_PATH,'trainset_files/')
TEST_SET_PATH =  os.path.join(NEW_DATASET_PATH,'testset_files/')
VALID_SET_PATH =  os.path.join(NEW_DATASET_PATH,'validset_files/')

JSON_CONFIG_PATH = '../../db-config/%s' % db_version
JSON_PATH = '%s/%s.json' % (JSON_CONFIG_PATH, NEW_DATASET_NAME)


######
# CONFIG PART FOR TRAINING RULES
WITH_R_R_R___ = '11088-r3-r4_11'



DATASET_NAME_FOR_TRAIN = '%s-renamedVariable-data-%s-template-with-HTML-for-D1' % (LANG, WITH_R_R_R___)
DATASET_FOR_TRAIN = '/absolute/path/db/%s/' % DATASET_NAME_FOR_TRAIN #I can used for word2vec like that


NEW_DATASET_NAME_FOR_TRAIN = '%s-renamedVariable-data-%s-template_ALL_IN_TRAINING-with-HTML-D1-%spc' % (LANG, WITH_R_R_R___, PERCENT_FOR_TRAIN)


NEW_DATASET_PATH_FOR_TRAIN = '../../../db/%s/%s' % (db_version, NEW_DATASET_NAME_FOR_TRAIN)

TRAIN_SET_PATH_FOR_TRAIN = os.path.join(NEW_DATASET_PATH_FOR_TRAIN,'trainset_files/')
TEST_SET_PATH_FOR_TRAIN =  os.path.join(NEW_DATASET_PATH_FOR_TRAIN,'testset_files/')
VALID_SET_PATH_FOR_TRAIN =  os.path.join(NEW_DATASET_PATH_FOR_TRAIN,'validset_files/')


JSON_PATH_FOR_TRAIN = '%s/%s.json' % (JSON_CONFIG_PATH, NEW_DATASET_NAME_FOR_TRAIN)

######
# DESTINATION FOLDER FOR THE MERGING BASH SCRIPT BETWEEN TRAIN AND TEST
STR_PERCENT = "-"+str(PERCENT)+"pc"
STR_PERCENT = "-"+str(PERCENT)+"pc"+ "-" + str(PERCENT_FOR_TRAIN)+"tpc"
data_version = "data-v1"

MERGE_FINAL_DATABASE_NAME = "NODEJS-%s-renamedVariable-with-r3-r4_11-template-ALL_IN_TRAINING__with-r0-r1-r2-r5_14-template-SPLIT_IN_TEST_VALID-with-HTML-D1%s" % (
(data_version, STR_PERCENT))
ROOT_MERGE_FINAL_DATABASE = "/absolute/path/db"

########################################################################################################################
####### DO IT FOR CREATE VALID AND THE TESTING  DATASET ####################################################################
percent_ = PERCENT / 100
purcentage_0 = 0

create_datasets_from(dataset=DATASET, purcentage_train=purcentage_0, purcentage_test=percent_, purcentage_valid=percent_,
                     func_to_divide_database_in_datasets=divide_database_in_3_sets, json_config_path=JSON_CONFIG_PATH,
                     train_set_path=TRAIN_SET_PATH, test_set_path=TEST_SET_PATH, valid_set_path=VALID_SET_PATH, json_path=JSON_PATH)

import subprocess

pass_arg=[]
pass_arg.append("ARGS_merge_dataset__renamedVariable-ALL_IN_TRAINING-with-HTML__and__renamedVariable-SPLIT_IN_TEST_VALID.sh")
pass_arg.append(os.path.join(NEW_DATASET_PATH, ""))



pass_arg.append("%s/%s/%s/" % (ROOT_MERGE_FINAL_DATABASE, db_version, MERGE_FINAL_DATABASE_NAME))

logging.info(pass_arg)
subprocess.check_call(pass_arg)

########################################################################################################################
####### DO IT FOR CREATE TRAIN  DATASET ################################################################

percent_ = PERCENT_FOR_TRAIN / 100



create_datasets_from(dataset=DATASET_FOR_TRAIN, purcentage_train=percent_, purcentage_test=purcentage_0, purcentage_valid=purcentage_0,
                     func_to_divide_database_in_datasets=divide_database_in_3_sets, json_config_path=JSON_CONFIG_PATH,
                     train_set_path=TRAIN_SET_PATH_FOR_TRAIN, test_set_path=TEST_SET_PATH_FOR_TRAIN, valid_set_path=VALID_SET_PATH_FOR_TRAIN, json_path=JSON_PATH_FOR_TRAIN)

pass_arg=[]
pass_arg.append("ARGS_merge_dataset__renamedVariable-ALL_IN_TRAINING-with-HTML__and__renamedVariable-SPLIT_IN_TEST_VALID.sh")
pass_arg.append(os.path.join(NEW_DATASET_PATH_FOR_TRAIN, ""))



pass_arg.append("%s/%s/%s/" % (ROOT_MERGE_FINAL_DATABASE, db_version, MERGE_FINAL_DATABASE_NAME))

logging.info(pass_arg)
subprocess.check_call(pass_arg)
